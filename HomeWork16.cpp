﻿#include <iostream>

using namespace std;

class Stack
{
private:
    int S;
    int N = 100;

    int* p = new int[N];

public:
    int size()
    {
        return S;
    }

    Stack() : S(0)
    {}

    void push(int i)
    {
        p[S] = i;
        if (S < N)
        {
           S++;
        }
    }

    int pop()
    {
        if (S > 0)
        {
         S--;
        }
        int i = p[S];
        return i;
    }
    ~Stack()
    {
        if (S = 0)
            delete[] p;
    }
};

int main()
{
    Stack v;
    v.size();
    cout << v.size() << ' ' << "Iznacalniy razmer:" << '\n';
    v.push(1);
    v.push(2);
    v.push(3);
    cout << v.size() << ' ' << "Razmer posle dobavleniya:" << '\n';
    cout << v.pop() << ' ' << "pop 1:" << '\n';
    cout << v.pop() << ' ' << "pop 2:" << '\n';
    cout << v.pop() << ' ' << "pop 3:" << '\n';
    v.pop();
    cout << v.size() << ' ' << "Finalniy razmer:" << '\n';
    return 0;
}